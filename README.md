# MkDocs Tooltips

A markdown extension that puts [hint.css](https://kushagra.dev/lab/hint/) in your pages.

## Basic usage

| Markdown | Output |
| ---------| ------ |
| `I like \[**Markdown**]{I'm a tooltip}.` | I like [**Markdown**]{I'm a tooltip}. |

## Demo

*Move the cursor over the elements.*

### Color

[💡]{💡 This is an information tooltip|bottom-right|info}[✅]{✅ This is a success tooltip|bottom-right|success}[⚠️]{⚠️ This is a warning tooltip|bottom-right|warning}
[❌]{❌ This is an error tooltip|bottom-right|error}
{: .icons .color}

### Size

[[small](){: .md-button .md-button--primary .disabled }]{You can show very very long sentences inside tooltips by using various available size variation classes.|small}
[[medium](){: .md-button .md-button--primary .disabled }]{You can show very very long sentences inside tooltips by using various available size variation classes.|medium}
[[large](){: .md-button .md-button--primary .disabled }]{You can show very very long sentences inside tooltips by using various available size variation classes.|large}

### Position

[↖️]{top-left|top-left}[⬆️]{top|top}[↗️]{top-right|top-right}
{: .icons .position}

[⬅️]{left|left}[▪️]{default (top)}[➡️]{right|right}
{: .icons .position}

[↙️]{bottom-left|bottom-left}[⬇️]{bottom|bottom}[↘️]{bottom-right|bottom-right}
{: .icons .position}

### Extras

<br/>
[[keep](){: .md-button .md-button--primary .disabled }]{A tooltip always displayed|always}
[[instant](){: .md-button .md-button--primary .disabled }]{A tooltip with animation disabled |no-animate}
[[bounce](){: .md-button .md-button--primary .disabled }]{A tooltip with an alternate bouncing animation|bounce}
[[rounded](){: .md-button .md-button--primary .disabled }]{A tooltip with rounded corners|rounded}

Want to learn more? Check the [options](./options.md) and [advanced uses](./advanced.md).
