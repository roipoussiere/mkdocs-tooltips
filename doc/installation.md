# Installation

## 1. Get mkdocs-tooltips

```bash
pip install mkdocs-tooltips
```

## 2. Get hint.css

You must then download the CSS file and save it in your own documentation directory in `doc/css/hint.min.css`.

[download CSS file](./css/hint.min.css){: .md-button .md-button--primary}

*right-click / save as...*

## 3. Update your `mkdocs.yml`

Add the plugin:

```yaml
plugins:
  - tooltips
```

And the CSS file:

```yaml
extra_css:
  - css/hint.min.css
```
