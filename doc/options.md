# Options

You can specify an option after a pipe (`|`) next to the tooltip content, like this:

| Markdown | Output |
| ---------| ------ |
| `I like \[**Markdown**]{I'm a tooltip|left}.` | I like [**Markdown**]{I'm a tooltip|left}. |

Options are cumulative:

| Markdown | Output |
| ---------| ------ |
| `I like \[**Markdown**]{I'm a tooltip|left|medium|success}.` | I like [**Markdown**]{I'm a tooltip|left|medium|success}. |

## Position

| Markdown                              | Output                             |
| --------------------------------------| ---------------------------------- |
| `\[top-left]{hello|top-left}`         | [top-left]{hello|top-left}         |
| `\[top]{hello|top}`                   | [top]{hello|top}                   |
| `\[top-right]{hello|top-right}`       | [top-right]{hello|top-right}       |
| `\[right]{hello|right}`               | [right]{hello|right}               |
| `\[bottom-right]{hello|bottom-right}` | [bottom-right]{hello|bottom-right} |
| `\[bottom]{hello|bottom}`             | [bottom]{hello|bottom}             |
| `\[bottom-left]{hello|bottom-left}`   | [bottom-left]{hello|bottom-left}   |
| `\[left]{hello|left}`                 | [left]{hello|left}                 |

The default position is `top`.

## Color

| Markdown                    | Output                   |
| ----------------------------| ------------------------ |
| `\[error]{hello|error}`     | [error]{hello|error}     |
| `\[warning]{hello|warning}` | [warning]{hello|warning} |
| `\[info]{hello|info}`       | [info]{hello|info}       |
| `\[success]{hello|success}` | [success]{hello|success} |

## Size

| Markdown                  | Output                 |
| --------------------------| ---------------------- |
| `\[small]{hello|small}`   | [small]{hello|small}   |
| `\[medium]{hello|medium}` | [medium]{hello|medium} |
| `\[large]{hello|large}`   | [large]{hello|large}   |

## Style

| Markdown                    | Output                   |
| ----------------------------| ------------------------ |
| `\[rounded]{hello|rounded}` | [rounded]{hello|rounded} |

## Animation

| Markdown                          | Output                         |
| ----------------------------------| ------------------------------ |
| `\[always]{hello|always}`         | [always]{hello|always}         |
| `\[no-animate]{hello|no-animate}` | [no-animate]{hello|no-animate} |
| `\[bounce]{hello|bounce}`         | [bounce]{hello|bounce}         |
