# Advanced use

## Add tooltips on images, links, etc.

You can put whatever you want in the container:

| Markdown | Output |
| ---------| ------ |
| `\[[a link](https://www.mkdocs.org)]{hello}` | [[a link](https://www.mkdocs.org)]{hello} |
| `\[![](diamond.png)]{hello}` | [![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAACqAAAAqgB7e92gQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAGJSURBVEiJtdaxSl1BEAbgb0VJNIYgBAtRCNgJaUylttqnEsHCB8kT5BHyBrELCdoFu2AVEBEsBAux0uJCVBSVtXCvrNd7ztl7wx0YOMz88/+7szvnnBBjNEgbGig7hrsFQwhj+IhQyBOxH2O8epmJ8ZljCeepqBc/x9ILvg7yFVz2Qd72S6zknKF9yCGEz/iOVw3t+IbDjtgCVtPzDdZijD+eWoR13Bas8LqzBdnuv2a4W6y3F7+B+8IWVAoksi8Z9j5xO+mhx7UCSeRnhj8ZwlZDz4sthPABn7LQFoxg+393gCkcZdhtjLSTo9jpVwDvcZDhdjD6bA4wjt1eBfAOfzPMLsarBm0Ce6UCeIM/WX4PE5WTnIomPQ5SrQBe43eWO8Rk7asiK57GcZVAuhi/svgxprty1dyKWZx2CNxhBptZ7BSzlTwNQzOHs5ozOcNcLUfBZM6j1YW8hfnG+iZAElnERUZ+gcWi2hJQEllOq25hubTu6XtQYiGEtxBj/Fdc04tAPzbwv4oHDSnHFsvXdO0AAAAASUVORK5CYII=)]{hello} |

## Add emojis in tooltips

Due to the fact that hint.css is a pure CSS library, it's not possible to add complex html content inside tooltips. But you can still use emojis:

| Markdown | Output |
| ---------| ------ |
| `\[I ❤️ emoji]{🐶🤗✨}` | [I ❤️ emoji]{🐶🤗✨} |

## Escaping

If the block starts with a `\`, the tooltip is not rendered:

| Markdown | Output |
| ---------| ------ |
| `You can escape \\[tooltips]{Hello!} if you need.` | You can escape \[tooltips]{Hello!} if you need. |

## Add custom CSS

All tooltip containers have the `tooltip` class. You can change their style with custom css:

```css
.tooltip {
  cursor:default;
}
```
