# Contributing

Woohoo thanks! 🎉

## Create issues

Please use the [issue tracker](https://framagit.org/roipoussiere/mkdocs-tooltips).

## Developing

### About dependencies

You may notice that there are two files where dependencies are defined:

- `pyproject.toml`: used to publish the `mkdocs-tooltips` module, which depends on the `mkdocs` module;
- `requirements.txt`: used to build the `mkdocs-tooltips` documentation website, which also depends on `mkdocs-material` and `mkdocs-tooltips`.

Generating a documentation that uses `mkdocs-tooltips` is not only good for demonstration purpose, it's also useful test the local version when developing.

### Install MkDocs Tooltips for development

```bash
git clone https://framagit.org/roipoussiere/mkdocs-tooltips.git
cd mkdocs-tooltips
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

You can re-build the local module and the local documentation each time you add modifications:

```bash
pip install . && mkdocs serve
```
